import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import {faEdit} from '@fortawesome/free-solid-svg-icons';



@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  faEdit = faEdit;

  @ViewChild("nome") nome!: ElementRef;
  nomeCompleto : string = "Matheus Correa"

  constructor() { }

  ngOnInit(): void {
  }

  enviarNovoNome(){
    this.nomeCompleto = this.nome.nativeElement.value;
  }

}
