import { PerfilModule } from './perfil/perfil.module';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MuralComponent } from './mural/mural.component';
import { DesempenhoComponent } from './desempenho/desempenho.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { LoginComponent } from './login/login.component';
import { EsqueceuSenhaComponent } from './login/esqueceu-senha/esqueceu-senha.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AutenticacaoService } from './login/autenticacao.service';
import { AuthGuard } from './guards/guard/auth.guard';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    MuralComponent,
    DesempenhoComponent,
    LoginComponent,
    EsqueceuSenhaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    GoogleChartsModule,
    PerfilModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AutenticacaoService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
