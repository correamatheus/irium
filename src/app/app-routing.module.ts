import { AuthGuard } from './guards/guard/auth.guard';
import { EsqueceuSenhaComponent } from './login/esqueceu-senha/esqueceu-senha.component';
import { LoginComponent } from './login/login.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MuralComponent } from './mural/mural.component';
import { DesempenhoComponent } from './desempenho/desempenho.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'mural',
  },
  {
    path: 'mural',
    component: MuralComponent,
    canActivate: [AuthGuard]
    
  },
  {
    path: 'desempenho',
    component: DesempenhoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'atividades',
    loadChildren: () => import("./atividades/atividades.module").then(m => m.AtividadesModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'disciplina/:id',
    loadChildren: () => import("./disciplina/disciplina.module").then(m => m.DisciplinaModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'esquecisenha',
    component: EsqueceuSenhaComponent    
  },
  {
    path: 'perfil',
    loadChildren: () => import("./perfil/perfil-routing.module").then(m => m.PerfilRoutingModule),
    
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
