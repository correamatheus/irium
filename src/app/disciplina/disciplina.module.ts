import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DisciplinaRoutingModule } from './disciplina-routing.module';
import { DisciplinaComponent } from './disciplina.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NotaComponent } from './nota/nota.component';
import { ProficienciaComponent } from './proficiencia/proficiencia.component';


@NgModule({
  declarations: [DisciplinaComponent, NotaComponent, ProficienciaComponent],
  imports: [
    CommonModule,
    DisciplinaRoutingModule,
    FontAwesomeModule
  ]
})
export class DisciplinaModule { }
