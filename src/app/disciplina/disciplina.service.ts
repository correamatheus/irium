import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DisciplinaService {

  constructor(private http: HttpClient) { }

  getDisciplinas() {
    return [
      {
        id: 1,
        nome: "Matemática",
        aula1: "Aula 1.1",
        conteudoAula1: "Funcao Quadratica",
        dataAula1: "30/08/2021",
        aula2: "Aula 1.2",
        conteudoAula2: "Trigonometria",
        dataAula2: "30/08/2021",
        aula3: "Aula 1.3",
        conteudoAula3: "Geometria Plana",
        dataAula3: "30/08/2021"
      },
      {
        id: 2,
        nome: "Biologia",
        aula1: "Aula 1.1",
        conteudoAula1: "Citoliga",
        dataAula1: "30/08/2021",
        aula2: "Aula 1.2",
        conteudoAula2: "Sistema Nervoso",
        dataAula2: "30/08/2021",
        aula3: "Aula 1.3",
        conteudoAula3: "Evolução",
        dataAula3: "30/08/2021"
      }


    ]


  }

  getDisciplina(id: number) {
    let disciplinas = this.getDisciplinas()
    for (let i = 0; i < disciplinas.length; i++) {
      let disciplina = disciplinas[i]
      if (disciplina.id == id) {
        return disciplina
      }

    }
    return null
  }


}
