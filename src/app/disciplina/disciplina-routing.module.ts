import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotaComponent } from './nota/nota.component';
import { DisciplinaComponent } from './disciplina.component';

const routes: Routes = [
  {
    path: '', component: DisciplinaComponent, children: [
      { path: 'nota', pathMatch: 'full', redirectTo: 'nota' }
    ]
  },
  {
    path: 'nota', component: NotaComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DisciplinaRoutingModule { }
