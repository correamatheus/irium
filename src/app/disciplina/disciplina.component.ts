import { DisciplinaService } from './disciplina.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {faClipboardCheck, faClipboard, faExclamationTriangle, faClipboardList } from '@fortawesome/free-solid-svg-icons'
import {faCircle} from '@fortawesome/free-solid-svg-icons'
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-disciplina',
  templateUrl: './disciplina.component.html',
  styleUrls: ['./disciplina.component.css']
})
export class DisciplinaComponent implements OnInit {

  faClipboardCheck = faClipboardCheck;
  faClipboard = faClipboard;
  faExclamationTriangle = faExclamationTriangle;
  faClipboardList = faClipboardList;
  faCircle = faCircle;
  
  // disciplinas!:any ;
  id!: number;
  inscricao!: Subscription
  disciplina!: any
  disciplinas!: any

  constructor(private route:ActivatedRoute, private disciplinaService: DisciplinaService) { }

  ngOnInit(): void {
    this.inscricao = this.route.params.subscribe(
      (params: any)=>{
        this.id = params['id'];
        this.disciplina = this.disciplinaService.getDisciplina(this.id)
        this.disciplinas = this.disciplinaService.getDisciplinas()    
      }
    )
      
    
    
    
  }
  
  
  
}
