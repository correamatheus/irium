import { RespostaLogin } from './RespostaLogin';
import { Injectable, Output } from '@angular/core';
import { Router } from '@angular/router';
import { EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './usuario';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutenticacaoService {

  usuarioAutenticado : boolean = false;
  

  // usuario! : Usuario

  mostrarMenuEmitter = new EventEmitter<boolean>();
  isUsuarioProfessor = new EventEmitter<boolean>();

  constructor(private router: Router, private http: HttpClient) { }

  fazerLoginAluno(usuario: Usuario){
    if(usuario.email === 'aluno@email.com' && usuario.senha === '12345678'){
      this.usuarioAutenticado = true;
      this.mostrarMenuEmitter.emit(true);
      this.router.navigate(['/mural'])

    }else if(usuario.email === 'professor@email.com' && usuario.senha === '12345678'){
      this.usuarioAutenticado = true;
      this.mostrarMenuEmitter.emit(true);
      this.isUsuarioProfessor.emit(true);
      this.router.navigate(['/mural'])
      
    }
     else {
      this.usuarioAutenticado = false;
      this.mostrarMenuEmitter.emit(false);
    }
  }



  usuarioEstaAutenticado(){
    return this.usuarioAutenticado;
  }
}
