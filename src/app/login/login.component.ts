import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AutenticacaoService } from './autenticacao.service';
import { Usuario } from './usuario';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  usuario = new Usuario()


  constructor(private autenticacaoService: AutenticacaoService) { }

  ngOnInit(){
   
  }


  fazerLogin(){
    this.autenticacaoService.fazerLoginAluno(this.usuario);
    console.log(this.usuario)
  }



  revelarsenha() {
    
    let input = document.querySelector('#password');
    if (input?.getAttribute('type') == 'password') {
      input.setAttribute('type', 'text');

      
    } else {
      input?.setAttribute('type', 'password')
    }


  }
}
