import { AtividadeSecundariaComponent } from './atividade-secundaria/atividade-secundaria.component';
import { AtividadesComponent } from './atividades.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', component: AtividadesComponent, children: [
    {path: 'atividade/:id', pathMatch: 'full', redirectTo: 'atividade/:id'}
  ]},
  {
    path: 'atividade/:id', component: AtividadeSecundariaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtividadesRoutingModule { }
