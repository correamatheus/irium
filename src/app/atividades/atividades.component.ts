import { Component, Input, OnInit } from '@angular/core';
import {faClipboardCheck, faClipboard, faExclamationTriangle, faClipboardList } from '@fortawesome/free-solid-svg-icons'
import { AutenticacaoService } from './../login/autenticacao.service';
import { AtividadesService } from './atividades.service';

@Component({
  selector: 'app-atividades',
  templateUrl: './atividades.component.html',
  styleUrls: ['./atividades.component.css']
})
export class AtividadesComponent implements OnInit {

  usuarioProfessor: boolean = false;

  faClipboardCheck = faClipboardCheck;
  faClipboard = faClipboard;
  faExclamationTriangle = faExclamationTriangle;
  faClipboardList = faClipboardList;

  atividadesSemana!:any[];
  atividadesProximaSemana!: any[];

  constructor(private atividadesService: AtividadesService, private autenticacaoService: AutenticacaoService) { }

  ngOnInit(){
    this.autenticacaoService.isUsuarioProfessor.subscribe(
      e => this.usuarioProfessor = e
    )
   this.getAtividade()
   this.getAtividadeProximaSemana()   
  
  }

  getAtividade(){
    this.atividadesSemana = this.atividadesService.getAtividadesSemana()
  }

  getAtividadeProximaSemana(){
    this.atividadesProximaSemana = this.atividadesService.getAtividadesProximaSemana()
    
  }
}
