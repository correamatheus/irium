import { AutenticacaoService } from './../login/autenticacao.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AtividadesRoutingModule } from './atividades-routing.module';
import { AtividadesComponent } from './atividades.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AtividadeSecundariaComponent } from './atividade-secundaria/atividade-secundaria.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [AtividadesComponent, AtividadeSecundariaComponent],
  imports: [
    CommonModule,
    AtividadesRoutingModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  exports: [],
  providers: [AutenticacaoService]
})
export class AtividadesModule { }
