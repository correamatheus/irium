import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtividadeSecundariaComponent } from './atividade-secundaria.component';

describe('AtividadeSecundariaComponent', () => {
  let component: AtividadeSecundariaComponent;
  let fixture: ComponentFixture<AtividadeSecundariaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtividadeSecundariaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtividadeSecundariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
