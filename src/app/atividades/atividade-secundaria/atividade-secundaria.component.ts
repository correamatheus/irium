import { AtividadesService } from './../atividades.service';
import { Component, OnInit, ɵɵsetComponentScope } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-atividade-secundaria',
  templateUrl: './atividade-secundaria.component.html',
  styleUrls: ['./atividade-secundaria.component.css']
})
export class AtividadeSecundariaComponent implements OnInit {

  id!: number;
  inscricao!: Subscription
  atividade!:any
  
  constructor(private route:ActivatedRoute, private atividadesService: AtividadesService) { 
  
  }

  

  ngOnInit(): void {
    this.inscricao = this.route.params.subscribe(
      (params: any)=>{
        this.id = params['id'];
        this.atividade = this.atividadesService.getAtividadeSemana(this.id)    
      }
    )

  
  }

  ngOnDestroy() {
    this.inscricao.unsubscribe()
  }

}
