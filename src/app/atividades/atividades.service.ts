import { Mural } from './../mural/Mural';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AtividadesService {


  constructor(private http: HttpClient) { }

  getAtividadesSemana() {
    return [
      {
        id: 1,
        disciplina: "História",
        descricao: "Breve descrição do que foi postado",
        data: "30/08/2021",
        diaSemana: "Segunda Feira",
        professor: "Professor",
        imagem: "../../assets/images/12.libro.jpg",
        status: "",
        aula: "Aula 1.1"
      },
      {
        id: 2,
        disciplina: "Matemática",
        descricao: "Breve descrição do que foi postado",
        data: "30/08/2021",
        diaSemana: "Segunda Feira",
        professor: "Professor",
        imagem: "../../assets/images/matematica.jpg",
        status: "",
        aula: "Aula 1.2"
      },
      {
        id: 3,
        disciplina: "Biologia",
        descricao: "Breve descrição do que foi postado",
        data: "30/08/2021",
        diaSemana: "Segunda Feira",
        professor: "Professor",
        imagem: "../../assets/images/biologia.jpg",
        status: "",
        aula: "Aula 2.1"
      },
      {
        id: 4,
        disciplina: "Física",
        descricao: "Breve descrição do que foi postado",
        data: "31/08/2021",
        diaSemana: "Terça Feira",
        professor: "Professor",
        imagem: "../../assets/images/12.libro.jpg",
        status: "",
        aula: "Aula 1.3"
      }
    ]
  }

  getAtividadesProximaSemana() {
    return [
      {
        id: 1,
        disciplina: "História",
        descricao: "Breve descrição do que foi postado",
        data: "06/09/2021",
        diaSemana: "Segunda Feira",
        professor: "Professor",
        imagem: "../../assets/images/12.libro.jpg",
        status: "",
        aula: "Aula 1.1"
      },
      {
        id: 2,
        disciplina: "Matemática",
        descricao: "Breve descrição do que foi postado",
        data: "06/09/2021",
        diaSemana: "Segunda Feira",
        professor: "Professor",
        imagem: "../../assets/images/matematica.jpg",
        status: "",
        aula: "Aula 1.2"
      },
      {
        id: 3,
        disciplina: "Biologia",
        descricao: "Breve descrição do que foi postado",
        data: "06/09/2021",
        diaSemana: "Segunda Feira",
        professor: "Professor",
        imagem: "../../assets/images/biologia.jpg",
        status: "",
        aula: "Aula 2.1"
      },
      {
        id: 4,
        disciplina: "Física",
        descricao: "Breve descrição do que foi postado",
        data: "07/09/2021",
        diaSemana: "Terça Feira",
        professor: "Professor",
        imagem: "../../assets/images/12.libro.jpg",
        status: "",
        aula: "Aula 1.3"
      }
    ]
  }

  // getAtividadesSemana(): Observable<any> {
  //   return this.http.get<any>("http://localhost:3000/atividadeSemana")
  // }

  // getAtividadesSemanaAnterior(): Observable<Mural> {
  //   return this.http.get<Mural>("http://localhost:3000/atividadeSemanaAnterior")
  // }

  getAtividadeSemana(id: number) {

    let atividades = this.getAtividadesSemana();
    for (let i = 0; i < atividades.length; i++) {
      let atividade = atividades[i]
      if (atividade.id == id) {
        return atividade
      }
    }
    return null

  }




}

  // getAtividadeProximaSemana(id: number){
  //   let atividades = this.getAtividadesProximaSemana();
  //   for(let i = 0; i < atividades.length; i++){
  //     let atividade = atividades[i]
  //     if(atividade.id == id){
  //       return atividade
  //     }      
  //   }
  //   return null
  // }

// }

