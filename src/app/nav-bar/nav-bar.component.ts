import { DisciplinaService } from './../disciplina/disciplina.service';
import { Component, OnInit } from '@angular/core';

import { faUserCircle, faTasks, faBars, faChartLine, faListAlt, faBook, faHeadset, faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

// Icones FontAwesome
faUserCircle = faUserCircle;
faBars = faBars;
faTasks = faTasks;
faChartLine = faChartLine;
faListAlt = faListAlt;
faBook = faBook;
faHeadset = faHeadset;
faQuestionCircle = faQuestionCircle;

disciplinas:any
 


  constructor(private disciplinaService: DisciplinaService, private route: ActivatedRoute) { }
  
 
  ngOnInit() {
    this.disciplinas = this.disciplinaService.getDisciplinas()
    
  }

}
