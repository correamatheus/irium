import { Observable } from 'rxjs';
import { MuralService } from './mural.service';
import { Component, OnInit } from '@angular/core';
import { Mural } from './Mural';

@Component({
  selector: 'app-mural',
  templateUrl: './mural.component.html',
  styleUrls: ['./mural.component.css']
})
export class MuralComponent implements OnInit {

  itemsSelect: string[] = ['Hoje', 'Esta semana', 'Este mês'];
  atividadesMural!: any[]

  

  constructor(private muralService: MuralService) { }

  ngOnInit(): void {
    this.atividadesMural = this.muralService.getAtividadesMural()
  }

  

 
}
