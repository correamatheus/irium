export class Mural {
    id!: number;
    disciplina!: string;
    descricao!: string;
    data!: string;
    diaSemana!: string
    professor!: string;
    imagem!: string;
    status!: string;
    aula!:string;
}