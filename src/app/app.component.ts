import { AutenticacaoService } from './login/autenticacao.service';
import { Component, Output } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hub-irium';

  mostrarMenu: boolean = false;
  

  constructor(private autenticacaoService: AutenticacaoService){

  }

  ngOnInit() {
    this.autenticacaoService.mostrarMenuEmitter.subscribe(
      mostrar => this.mostrarMenu = mostrar
    )

    
  }

}
